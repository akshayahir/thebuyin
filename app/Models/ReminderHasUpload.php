<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReminderHasUpload extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'reminder_id','parent_id','child_id','video'
    ];
}
