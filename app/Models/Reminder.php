<?php
namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reminder extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'reminder_name','parent_id','child_id','exercise_id','duration_id','reminder_status','next_reminder_datetime','minutes'
    ];

    public function exercise(){
        return $this->hasOne(Exercise::class,'id','exercise_id');
    }

    public function duration(){
        return $this->hasOne(Duration::class,'id','duration_id');
    }

    public function parent(){
        return $this->hasOne(User::class,'id','parent_id');
    }

    public function child(){
        return $this->hasOne(User::class,'id','child_id');
    }

    public function video(){
        return $this->hasOne(ReminderHasUpload::class,'reminder_id','id');
    }
}
