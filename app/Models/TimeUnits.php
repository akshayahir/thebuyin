<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class TimeUnits extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'title'
    ];
}
