<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    //
    public function index(Request $request){
        $data=User::withTrashed()
            ->where('usertype','P')
            ->orderBy('created_at', 'desc')
            ->get();

        if($request->ajax()){
            return DataTables::of($data)
                ->setRowId(function ($data) {
                    return $data->id;
                })
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $output = '';
                    $output='
                        <div>
                            <a href='. route('user.child',["id"=>$data->id]) .' id="" class="btn bg-gradient-primary"><i class="fa fa-eye"></i>&nbsp;</a>
                        </div>
                        ';

                    return $output;
                })
                ->rawColumns(['action'])
                ->blacklist(['action'])
                ->make(true);
        }

        return view('user.index');
    }

    public function view_child(Request $request,$id){
        $data=User::where('parent_id',$id)
            ->where('usertype',"C")
            ->get();

        if($request->ajax()){
            return DataTables::of($data)
                ->setRowId(function ($data) {
                    return $data->id;
                })
                ->addIndexColumn()
                ->make(true);
        }

        return view('user.child',[
            "id"=>$id
        ]);
    }
}
