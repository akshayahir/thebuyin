<?php

namespace App\Http\Controllers;

use App\Models\Duration;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class DurationController extends Controller
{
    public function index(Request $request){
        $data=Duration::withTrashed()
            ->get();

        if($request->ajax()){
            return DataTables::of($data)
                ->setRowId(function ($data) {
                    return $data->id;
                })
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $output = '';
                    $output='
                        <div>
                            <button id="" class="btn bg-gradient-primary edit_model"
                            data-id="'. $data->id .'" data-time="'.$data->time_unit_id.'"
                            data-duration="'. $data->duration .'">

                            <i class="fa fa-edit"></i>&nbsp; Edit </button>

                        ';

                    if ($data->deleted_at != NULL) {
                        $output.='
                            <button id="" class="btn  bg-gradient-danger restore_model"
                            data-id="'. $data->id .'">

                            <i class="fa fa-undo"></i>&nbsp; Restore </button>
                       ';
                    }else{
                        $output.='
                            <button id="" class="btn bg-gradient-danger delete_model"
                            data-id="'. $data->id .'">

                            <i class="fa fa-trash"></i>&nbsp; Delete </button>
                       ';
                    }

                    $output.='</div>';

                    return $output;
                })
                ->addColumn('status', function ($data) {
                    if ($data->deleted_at == null) {
                        //return $data->status_id;
                        return "<span class='bg-success  color-palette'> Active </span>";
                    } else {
                        return "<span class='bg-danger color-palette'> Deleted </span>";
                    }
                })
                ->addColumn('time_unit', function ($data) {
                    if ($data->time_unit_id==1) {
                        return "Minutes";
                    } else {
                        return "Hours";
                    }
                })
                ->rawColumns(['action','status'])
                ->blacklist(['action','status'])
                ->make(true);
        }

        return view('duration.index');
    }

    public function add(Request $request){
        $validator=Validator::make($request->all(),[
            'time_unit'=>'required|not_in:0',
            'duration'=>'required'
        ]);

        if ($validator->fails()) {
            $message = "<ul id='add-alert' class='alert alert-danger' style='padding-left:30px;'>";

            foreach ($validator->errors()->all() as $error) {
                $message .= "<li>$error</li>";
            }

            $message .= "</ul>";

            return response()->json([
                'message' => $message,
                'success' => 0
            ]);
        }

        $exercise=new Duration();

        $exercise->time_unit_id=$request->time_unit;
        $exercise->duration=$request->duration;

        $exercise->save();

        return response()->json([
            'success' => 1
        ]);
    }
    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            'time_unit'=>'required|not_in:0',
            'duration'=>'required',
             'id'=>'required'
        ]);

        if ($validator->fails()) {
            $message = "<ul id='add-alert' class='alert alert-danger' style='padding-left:30px;'>";

            foreach ($validator->errors()->all() as $error) {
                $message .= "<li>$error</li>";
            }

            $message .= "</ul>";

            return response()->json([
                'message' => $message,
                'success' => 0
            ]);
        }

        $exercise=Duration::find($request->id);

        $exercise->time_unit_id=$request->time_unit;
        $exercise->duration=$request->duration;

        $exercise->save();

        return response()->json([
            'success' => 1
        ]);

    }

    public function delete(Request $request){
        Duration::withTrashed()->where('id',$request->id)->delete();

        return response()->json([
            'success' => 1
        ]);
    }

    public function restore(Request $request){
        Duration::withTrashed()->where('id',$request->id)->restore();

        return response()->json([
            'success' => 1
        ]);
    }

}
