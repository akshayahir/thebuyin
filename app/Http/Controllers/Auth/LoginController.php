<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest')->except('index', 'logout', 'postLogout');
    }
    public function index() {
        $users = Auth::user();
        if (!empty($users)) {
            return redirect()->route('dashboard');
        }
        return view('auth.login');
    }

    public function register() {
        return redirect()->route('loginpage');
    }
     public function postLogin(Request $request) {
        $email = $request->email;
        $password = $request->password;
        
        if ($email && $password) {
            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                $users = Auth::user();  
                    if ($users->status == "1") {                                                
                        Session::put('selected_user_id', $users->id);
                        Session::put('selected_role_id', $users->roll_id);
                                              
                        return redirect()->route('dashboard')->with('success', trans('common.label_welcome_msg') . $users->last_name . ' ' . $users->first_name);
                    } else {
                        Auth::logout();
                        return redirect()->route('loginpage')->with('error', trans('common.label_common_status_inactive'));
                    }
            } else {
                return redirect()->route('loginpage')->with('error', trans('auth.failed'));
            }
        } else {
            return redirect()->route('loginpage')->with('error', trans('common.label_common_error'));
        }
    }

    public function postLogout(Request $request) {
        Auth::logout();
        $request->session()->flush();
        return redirect()->route('loginpage');
    }
}
