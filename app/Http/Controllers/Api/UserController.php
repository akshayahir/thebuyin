<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth,
    DB,
    Config,
    Mail;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\URL;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends ApiCommonController
{

    private $folder = 'uploads/video/';

    public function register(Request $request)
    {

        $appname = Config::get('constants.AppName');
        $data = $request->all();
        $id = '0';
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile_number' => 'required|unique:users,mobile_number,' . $id . ',id,deleted_at,NULL',
            'email' => 'required|unique:users,email,' . $id . ',id,deleted_at,NULL',
            'password' => 'required|min:8'
        ]);

        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $response = array('code' => 0, 'message' => $message, 'token' => '', 'data' => (object) []);
            return response()->json($response, 200);
        }

        $data['password'] = Hash::make($data['password']);
        $email_otp = ApiCommonController::generateOTP();
        $data['email_otp_verified'] = "0";
        $data['email_otp'] = $email_otp;
        $res = User::create($data);
        
        UserController::sentRegisterOTP($request->email, $data['email_otp'], trans('common.REGISTRATION_SUBJECT'));
        
        $data['id'] = $res->id;


        $token = $res->createToken($appname)->accessToken;

        $data_arr = $res->toArray();
        $return_data = ApiCommonController::removeNullValue($data_arr);
        return response()->json(['code' => 1, 'message' => 'Registered successfully. OTP has been sent to your email.', 'token' => $token, 'data' => $return_data], 200);
    }

        public static function sentRegisterOTP($email,$otp,$subject) 
    { 
       
        $email_data = array();
        $user_data = User::where('email',$email)->first();
        
       
        $email_data['email']=$email;
        $email_data['otp']=$otp;
        $email_data['name']=$user_data->first_name.' '.$user_data->last_name;
        
        $val = Mail::send('mailformat/userverification', $email_data, function($message) use($email,$subject) {
			$message->to($email);
			$message->subject($subject);
			$message->from(config('constants.EMAIL_FROM'));
        });
        return $val;
        
    }
    
    public function verify_otp(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'otp' => 'required'
        ]);

        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $response = array('code' => 0, 'message' => $message, 'token' => '', 'data' => (object) []);
            return response()->json($response, 200);
        }

        $user_data = User::where('email', $request->email)->where('email_otp', $request->otp)->first();
        if (!empty($user_data)) {
            $user_data->update(['email_otp_verified' => '1', 'email_otp' => ""]);
            $temp_data = $user_data->toArray();
            $return_data = ApiCommonController::removeNullValue($temp_data);
            $response = array('code' => 1, 'message' => "OTP is successfully Verified.", 'data' => $return_data);
            return response()->json($response, 200);
        } else {
            $response = array('code' => 0, 'message' => "Please enter valid OTP", 'data' => (object) []);
            return response()->json($response, 200);
        }
    }

    //end of function

    public function resend_otp(Request $request)
    {

        $user_details = User::select('id', 'first_name', 'last_name', 'mobile_number', 'email', 'email_otp', 'email_otp_verified')->where('email', $request->email)->first();

        if (!empty($user_details)) {
            $email_otp = ApiCommonController::generateOTP();
            UserController::sentFrogotPaaswordOTP($request->email, $email_otp, trans('common.FORGOTPASSWORD_SUBJECT'). ' - Resend OTP');
            $user_details->update(['email_otp' => $email_otp, 'email_otp_verified' => "0"]);
            return response()->json(["code" => 1, "message" => "OTP sent successfully. Please check your email", "data" => $user_details]);
        } else {
            return response()->json(["code" => 0, "message" => "User does not exists", "data" => (object) []]);
        }
    } //end of function

    
    public function login(Request $request)
    {
        
     
        $appname = Config::get('constants.AppName');
        $password = $request->password;
        $email = $request->email;

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $response = array('code' => 0, 'message' => $message, 'token' => '', 'data' => (object) []);
            return response()->json($response, 200);
        }

        $credentials = ['email' => $email, 'password' => $password];

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            if (empty($user->email_otp_verified)) {
                $token = $user->createToken($appname)->accessToken;
                $data = $user->toArray();
                $return_data = ApiCommonController::removeNullValue($data);
                return response()->json(['code' => 0, 'message' => 'OTP is not verified. Please check your email for OTP', 'token' => $token, 'data' => $return_data], 200);
            }
            Auth::user()->AauthAcessToken()->delete();
            $temp_para = array();
            $temp_para['device_type'] = $request->device_type;
            $temp_para['device_id'] = $request->device_id;
            $user->update($temp_para);

            $token = $user->createToken($appname)->accessToken;
            $data = $user->toArray();
            $return_data = ApiCommonController::removeNullValue($data);

            return response()->json(['code' => 1, 'message' => 'User login successfully', 'token' => $token, 'data' => $return_data], 200);
        } else {
            return response()->json(['code' => 0, 'message' => 'Invalid email / password',  'token' => '', 'data' => (object) []], 200);
        }
    }

    //end of function


    public function profile(Request $request)
    {
        $user = Auth::user();
        $id = $user->id;

        $user_data = User::select('*')->where("id", $id)->first();

        $temp_data = $user_data->toArray();
        $return_data = ApiCommonController::removeNullValue($temp_data);
        return response()->json(['code' => 1, 'message' => "Success", "data" => $return_data], 200);
    }

    public function profileSetup(Request $request)
    {

        $user = Auth::user();
        $id = $user->id;

        $data = $request->all();

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile_number' => 'required|unique:users,mobile_number,' . $id . ',id,deleted_at,NULL',
        ]);

        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $response = array('code' => 0, 'message' => $message, 'token' => '', 'data' => (object) []);
            return response()->json($response, 200);
        }

        $profile_setup = User::find($id);
        $profile_setup->first_name = $data['first_name'];
        $profile_setup->last_name = $data['last_name'];
        $profile_setup->mobile_number = $data['mobile_number'];
        $profile_setup->save();
        $temp_data = $profile_setup->toArray();

        $return_data = ApiCommonController::removeNullValue($temp_data);
        return response()->json(["code" => 1, "message" => "Profile updated successfully", 'data' => $return_data]);
    }




    public function change_password(Request $request)
    {
        $user = Auth::user();
        $id = $user->id;

        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'password' => 'required|min:8',
        ]);
        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $response = array('code' => 0, 'message' => $message, 'data' => (object) []);
            return response()->json($response, 200);
        }

        $old_password = $request->input("old_password");
        $password = $request->input("password");


        if (Hash::check($old_password, $user->password)) {
            $user->update(['password' => Hash::make($password)]);
            $data['id'] = $id;
            return response()->json(['code' => 1, 'message' => "Password changed successfully", "data" => $data], 200);
        } else {
            return response()->json(['code' => 0, 'message' => "Old Password is Incorrect", 'data' => (object) []], 200);
        }
    }

    public function forgotpassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required'
        ]);

        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $response = array('code' => 0, 'message' => $message, 'token' => '', 'data' => (object) []);
            return response()->json($response, 200);
        }
        $email = $request->email;
        $data = [];
        $user_details = User::select('id', 'email_otp', 'first_name', 'last_name', 'mobile_number', 'email')->where('email', $email)->first();

        if (!empty($user_details)) {

            $email_otp = ApiCommonController::generateOTP();
            UserController::sentFrogotPaaswordOTP($email, $email_otp, trans('common.FORGOTPASSWORD_SUBJECT'));
            $user_details->update(['email_otp' => $email_otp]);
            
            $return_data = ['email_otp' => $email_otp, 'id' => $user_details->id, 'first_name' => $user_details->first_name, 'last_name' => $user_details->last_name, 'mobile_number' => $user_details->mobile_number, 'email' => $user_details->email];
            return response()->json(["code" => 1, "message" => "Please verify OTP for reset password", 'data' => $return_data], 200);
        } else {
            return response()->json(["code" => 0, "message" => "User does not exists", 'data' => (object) $data], 200);
        }
    } //end of function

    public function resetpassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:8',
            'userid' => 'required',
        ]);
        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $response = array('code' => 0, 'message' => $message, 'token' => '', 'data' => (object) []);
            return response()->json($response, 200);
        }
        $user_details = User::select('id', 'first_name', 'last_name', 'mobile_number', 'email', 'password')->where('id', $request->userid)->first();

        if (!empty($user_details)) {
            $user_details->update(['password' => Hash::make($request->password)]);
            $return_data = ['id' => $user_details->id, 'first_name' => $user_details->first_name, 'last_name' => $user_details->last_name, 'mobile_number' => $user_details->mobile_number, 'email' => $user_details->email];
            return response()->json(["code" => 1, "message" => "Password changed successfully", "data" => $return_data], 200);
        } else {
            return response()->json(["code" => 0, "message" => "User does not exists", "data" => (object) []], 200);
        }
    } //end of function

    public function logout(Request $request)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $user->update(['device_id' => '']);
            $user = Auth::user()->token();
            $user->revoke();
        } 
        return response()->json(['code' => 1, 'message' => "Successfully Logout!", 'data' => (object) []], 200);
    }


    public function addChild(Request $request)
    {
        
        $child_id = $request->child_id;
        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'mobile_number' => 'required|unique:users,mobile_number,' . $child_id . ',id,deleted_at,NULL',
            'email' => 'required|unique:users,email,' . $child_id . ',id,deleted_at,NULL',
        ]);

        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $response = array('code' => 0, 'message' => $message, 'token' => '', 'data' => (object) []);
            return response()->json($response, 200);
        }

        
         if(empty($child_id)){

            $validator = Validator::make($request->all(), [
                'password' => 'required|min:8'
            ]);

            if ($validator->fails()) {
                $message = implode(", ", $validator->messages()->all());
                $response = array('code' => 0, 'message' => $message, 'token' => '', 'data' => (object) []);
                return response()->json($response, 200);
            }


            $user = Auth::user();

            $data['parent_id'] = $user->id;
            $data['usertype'] = config('constants.USER_TYPE_CHILD');
            $data['password'] = Hash::make($data['password']);
            $data['email_otp_verified'] = "1";

            $res = User::create($data);
            $data['id'] = $res->id;

            $data_arr = $res->toArray();
            $return_data = ApiCommonController::removeNullValue($data_arr);
            return response()->json(['code' => 1, 'message' => 'New child create successfully.', 'token' => '', 'data' => $return_data], 200);
        } else {

            $u = User::find($child_id);

            if (!empty($u)) {

                $user = Auth::user();

                $password = $request->password;
                if (!empty($password)) {
                    $u->password = Hash::make($data['password']);
                }
                
                $u->first_name = $data['first_name'];
                $u->last_name = $data['last_name'];
                $u->mobile_number = $data['mobile_number'];
                $u->email = $data['email'];
                $u->save();
               

                $data_arr = $u->toArray();
                $return_data = ApiCommonController::removeNullValue($data_arr);
                return response()->json(['code' => 1, 'message' => 'child updated successfully.', 'token' => '', 'data' => $return_data], 200);
            }
            else{
                return response()->json(['code' => 0, 'message' => 'Child Not found.', 'token' => ''], 200);
            }
        }
    }

    public function deleteChild(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'child_id' => 'required'
        ]);

        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $response = array('code' => 0, 'message' => $message, 'token' => '', 'data' => (object) []);
            return response()->json($response, 200);
        }
        $res = User::where('id', $request->child_id)
            ->where('usertype', 'C')
            ->delete();
        $return_data['child_id'] = $request->child_id;
        return response()->json(['code' => 1, 'message' => 'Child user deleted.', 'token' => 'successfully', 'data' => $return_data], 200);
    }

    public function getExercises()
    {
        $res = \App\Models\Exercise::select('id', 'exercise_name')->orderBy('exercise_name', 'asc')->get();
        if ($res->count()) {
            $return_data = $res->toArray();
            return response()->json(['code' => 1, 'message' => 'Exercises list.', 'token' => '', 'data' => $return_data], 200);
        } else {
            return response()->json(['code' => 1, 'message' => 'No exercise found.', 'token' => '', 'data' => []], 200);
        }
    }

    public function getDurationTime()
    {
        $res = \App\Models\Duration::select('durations.id', 'durations.duration', 'time_units.title')
            ->leftjoin('time_units', 'time_units.id', '=', 'durations.time_unit_id')
            ->orderBy('durations.time_unit_id', 'asc')->orderBy('durations.duration', 'asc')->get();
        if ($res->count()) {
            $return_data = $res->toArray();
            return response()->json(['code' => 1, 'message' => 'Reminders list.', 'token' => '', 'data' => $return_data], 200);
        } else {
            return response()->json(['code' => 1, 'message' => 'No reminder found.', 'token' => '', 'data' => []], 200);
        }
    }

    public function getChildren()
    {
        $user = Auth::user();
        $res = User::select('id', 'first_name', 'last_name', 'mobile_number', 'email')->where('parent_id', $user->id)->where('usertype', config('constants.USER_TYPE_CHILD'))->orderBy('first_name', 'asc')->get();
        if ($res->count()) {
            $return_data = $res->toArray();
            return response()->json(['code' => 1, 'message' => 'Children list.', 'token' => '', 'data' => $return_data], 200);
        } else {
            return response()->json(['code' => 1, 'message' => 'No child found.', 'token' => '', 'data' => []], 200);
        }
    }

    public function addReminder(Request $request)
    {

        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'reminder_name' => 'required',
            'child_id' => 'required|gt:0',
            'exercise_id' => 'required|gt:0',
            'duration_id' => 'required|gt:0',
        ]);

        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $response = array('code' => 0, 'message' => $message, 'token' => '', 'data' => []);
            return response()->json($response, 200);
        }

        $user = Auth::user();

        if ($user->usertype != config('constants.USER_TYPE_PARENT')) {
            return response()->json(['code' => 0, 'message' => 'Only parent user can create reminder.', 'token' => '', 'data' =>  []], 200);
        }

        $data['parent_id'] = $user->id;
        
        
        $get_duration = \App\Models\Duration::select('*')->where('id',$request->duration_id)->first();
        if(!empty($get_duration)){
            $minutes = '';
            if($get_duration->time_unit_id == config('constants.COMMON_MINUTES')){
                $minutes = $get_duration->duration;
            } else {
                $minutes = 60 * $get_duration->duration;
            }
            if($minutes){
                $current_date = date('Y-m-d H:i');
                $next_reminder_datetime =  date('Y-m-d H:i:s',strtotime('+'.$minutes.' minutes',strtotime($current_date)));
                $data['minutes'] = $minutes;
                $data['next_reminder_datetime'] = $next_reminder_datetime;
            }
            
        }
        
        $res = \App\Models\Reminder::create($data);
        $data['id'] = $res->id;

        
        $userdata = \App\Models\Reminder::select('reminders.id','exercises.exercise_name','users.device_id','users.id as child_id')
                ->leftjoin('exercises','exercises.id','=','reminders.exercise_id')
                ->leftjoin('users','users.id','=','reminders.child_id')
                ->where('reminders.id',$res->id)
                ->first();
                
        if (!empty($userdata)) {
            log::info('Notificaion: '. $userdata->child_id);
            $push_notification = array();
            $registrationIds = array($userdata->device_id);
            $push_notification['not_title'] = trans('common.exercises_created');
            $push_notification['not_msg'] = $user->first_name . ' ' . trans('common.exercises_created_not').' '.$userdata->exercise_name;
            $push_notification['registrationIds'] = $registrationIds;
            $push_notification['reminder_id'] = $res->id;
//            log::info(print_r($push_notification,true));
            ApiCommonController::pushNotification($push_notification);
        }
        
        $page_no = 1;
        $reminder_id = $data['id'];
        $temp_data = ApiCommonController::getReminderData($page_no,$user,$reminder_id);
        
        return response()->json(['code' => 1, 'message' => 'Reminder added successfully.', 'token' => '', 'page_no' => (int) $temp_data['page_no'], 'total_records' => (int) $temp_data['total_records'], 'data' => $temp_data['return_data']], 200);
    }

    public function updateReminder(Request $request)
    {

        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'reminder_id' => 'required|gt:0',
            'reminder_name' => 'required',
            'child_id' => 'required|gt:0',
            'exercise_id' => 'required|gt:0',
            'duration_id' => 'required|gt:0',
        ]);

        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $response = array('code' => 0, 'message' => $message, 'token' => '', 'data' => []);
            return response()->json($response, 200);
        }

        $user = Auth::user();

        if ($user->usertype != config('constants.USER_TYPE_PARENT')) {
            return response()->json(['code' => 0, 'message' => 'Only parent user can update reminder.', 'token' => '', 'data' => []], 200);
        }

        $reminder_status = \App\Models\Reminder::select('*')->where('parent_id', $user->id)->where('id', $data['reminder_id'])->first();

        if (empty($reminder_status)) {
            return response()->json(['code' => 0, 'message' => 'Reminder not found.', 'token' => '', 'data' => []], 200);
        }

        if (!empty($reminder_status->reminder_status)) {
            return response()->json(['code' => 0, 'message' => 'Reminder already completed. Hence can not update.', 'token' => '', 'data' =>[]], 200);
        }


        $get_duration = \App\Models\Duration::select('*')->where('id',$request->duration_id)->first();
        if (!empty($get_duration)) {
            $minutes = '';
            if ($get_duration->time_unit_id == config('constants.COMMON_MINUTES')) {
                $minutes = $get_duration->duration;
            } else {
                $minutes = 60 * $get_duration->duration;
            }
            
            if ($minutes) {
                $current_date = date('Y-m-d H:i');
                $next_reminder_datetime = date('Y-m-d H:i:s', strtotime('+' . $minutes . ' minutes', strtotime($current_date)));
                $reminder_status->minutes = $minutes;
                $reminder_status->next_reminder_datetime = $next_reminder_datetime;
                
                $userdata = \App\Models\Reminder::select('reminders.id','exercises.exercise_name','users.device_id','users.id as child_id')
                ->leftjoin('exercises','exercises.id','=','reminders.exercise_id')
                ->leftjoin('users','users.id','=','reminders.child_id')
                ->where('reminders.id',$data['reminder_id'])
                ->first();
//        
                if (!empty($userdata) && !empty($userdata->device_id)) {
                    $push_notification = array();
                    $registrationIds = array($userdata->device_id);
                    $push_notification['not_title'] = trans('common.exercises_updated');
                    $push_notification['not_msg'] = $user->first_name . ' ' . trans('common.exercises_updated_not').' '.$userdata->exercise_name;
                    $push_notification['registrationIds'] = $registrationIds;
                    $push_notification['reminder_id'] = $reminder_status->id;
                    ApiCommonController::pushNotification($push_notification);
                }
            }
        }



        $reminder_status->reminder_name = $data['reminder_name'];
        $reminder_status->child_id = $data['child_id'];
        $reminder_status->exercise_id = $data['exercise_id'];
        $reminder_status->duration_id = $data['duration_id'];
        $reminder_status->save();

        
        $page_no = 1;
        $reminder_id = $data['reminder_id'];
        $temp_data = ApiCommonController::getReminderData($page_no,$user,$reminder_id);
        
        return response()->json(['code' => 1, 'message' => 'Reminder updated successfully.', 'token' => '', 'page_no' => (int) $temp_data['page_no'], 'total_records' => (int) $temp_data['total_records'], 'data' => $temp_data['return_data']], 200);
        
//        
//        return response()->json(['code' => 1, 'message' => 'Reminder updated successfully.', 'token' => '', 'data' => $reminder_status], 200);
    }

    public function deleteReminder(Request $request)
    {

        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'reminder_id' => 'required'
        ]);

        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $response = array('code' => 0, 'message' => $message, 'token' => '', 'data' => []);
            return response()->json($response, 200);
        }

        $user = Auth::user();

        if ($user->usertype != config('constants.USER_TYPE_PARENT')) {
            return response()->json(['code' => 0, 'message' => 'You are not authorized to delete reminder.', 'token' => '', 'data' => []], 200);
        }

        $reminder_status = \App\Models\Reminder::select('id', 'reminder_status','child_id')->where('parent_id', $user->id)->where('id', $data['reminder_id'])->first();

        if (empty($reminder_status)) {
            return response()->json(['code' => 0, 'message' => 'Reminder not found.', 'token' => '', 'data' => []], 200);
        }

        if (!empty($reminder_status->reminder_status)) {
            return response()->json(['code' => 0, 'message' => 'Reminder already completed. Hence can not delete.', 'token' => '', 'data' => []], 200);
        }

        $query = \App\Models\Reminder::where('id', $data['reminder_id'])->delete();
        
        $userdata = \App\Models\Reminder::select('reminders.id','exercises.exercise_name','users.device_id','users.id as child_id')
                ->leftjoin('exercises','exercises.id','=','reminders.exercise_id')
                ->leftjoin('users','users.id','=','reminders.child_id')
                ->where('reminders.id',$data['reminder_id'])
                ->withTrashed()->first();

        if (!empty($userdata) && !empty($userdata->device_id)) {
            $push_notification = array();
            $registrationIds = array($userdata->device_id);
            $push_notification['not_title'] = trans('common.exercises_deleted');
            $push_notification['not_msg'] = $user->first_name . ' ' . trans('common.exercises_deleted_not').' '.$userdata->exercise_name;
            $push_notification['registrationIds'] = $registrationIds;
            $push_notification['reminder_id'] = $reminder_status->id;
            ApiCommonController::pushNotification($push_notification);
        }
        
        $page_no = 1;
        $temp_data = ApiCommonController::getReminderData($page_no,$user,'');
        
        return response()->json(['code' => 1, 'message' => 'Reminder deleted successfully.', 'token' => '', 'page_no' => (int) $temp_data['page_no'], 'total_records' => (int) $temp_data['total_records'], 'data' => $temp_data['return_data']], 200);
    }

    public function getChildPendingReminder()
    {

        $user = Auth::user();

        if ($user->usertype != config('constants.USER_TYPE_CHILD')) {
            return response()->json(['code' => 0, 'message' => 'Only child user can access.', 'token' => '', 'data' => []], 200);
        }
        $res = \App\Models\Reminder::select('reminders.*', 'exercises.exercise_name', 'durations.duration', 'time_units.title')
            ->leftjoin('exercises', 'reminders.exercise_id', '=', 'exercises.id')
            ->leftjoin('durations', 'reminders.duration_id', '=', 'durations.id')
            ->leftjoin('time_units', 'time_units.id', '=', 'durations.time_unit_id')
            ->where('reminder_status', trans('common.REMINDER_STATUS_PENDING'))
            ->where('child_id', $user->id)
            ->orderBy('reminders.id', 'desc')
            ->get();
        if ($res->count()) {
            foreach ($res as $key => $t_data) {
                $temp_data = $t_data->toArray();
                $return_data[$key] = ApiCommonController::removeNullValue($temp_data);
            }
            return response()->json(['code' => 1, 'message' => 'Pending reminders list.', 'token' => '', 'data' => $return_data], 200);
        } else {
            return response()->json(['code' => 1, 'message' => 'No reminder pending.', 'token' => '', 'data' => []], 200);
        }
    }

    public function uploadVideoReminder(Request $request)
    {

        $data = $request->all();
        $validator = Validator::make($request->all(), [
            'reminder_id' => 'required|gt:0',
            'video' => 'required'
        ]);

        if ($validator->fails()) {
            $message = implode(", ", $validator->messages()->all());
            $response = array('code' => 0, 'message' => $message, 'token' => '', 'data' => (object) []);
            return response()->json($response, 200);
        }

        $user = Auth::user();
        if ($user->usertype != config('constants.USER_TYPE_CHILD')) {
            return response()->json(['code' => 0, 'message' => 'Only child can upload video in reminder.', 'token' => '', 'data' => (object) []], 200);
        }

        $reminder_status = \App\Models\Reminder::find($data['reminder_id']);
        // only self child reminder can completed.
        if (empty($reminder_status) ||  $reminder_status->child_id != $user->id) {
            return response()->json(['code' => 0, 'message' => 'Reminder not found.', 'token' => '', 'data' => (object) []], 200);
        }

        // if reminder already completed
        if (!empty($reminder_status->reminder_status)) {
            return response()->json(['code' => 0, 'message' => 'Reminder already completed.', 'token' => '', 'data' => (object) []], 200);
        }

        if ($request->hasFile('video')) {
            $folder = 'uploads/video/';
            if (!file_exists('uploads/video/')) {
                mkdir('uploads/video/', 0777, true);
            }
            $video = ApiCommonController::uploadVideo('video', $request, $folder);
            $data['video'] = $video;
        }

        $data['child_id'] = $user->id;
        $data['parent_id'] = $user->parent_id;
        $res = \App\Models\ReminderHasUpload::create($data);

        $reminder_status->reminder_status = trans('common.REMINDER_STATUS_COMPLETED');
        $reminder_status->save();

        return response()->json(['code' => 1, 'message' => 'Reminder completed.', 'token' => '', 'data' => $reminder_status], 200);
    }

    public function getAllReminderList(Request $request){
        $page_no = $request->page_no;
        if (empty($page_no)) {
            $page_no = 1;
        }
        $user = Auth::user();
        $temp_data = ApiCommonController::getReminderData($page_no,$user,'');
        
        return response()->json(['code' => 1, 'message' => 'All reminders list.', 'token' => '', 'page_no' => (int) $temp_data['page_no'], 'total_records' => (int) $temp_data['total_records'], 'data' => $temp_data['return_data']], 200);
        
    }
    
   
     public static function sentFrogotPaaswordOTP($email,$otp,$subject) 
    { 
       
        $email_data = array();
        $user_data = User::where('email',$email)->first();
        
       
        $email_data['email']=$email;
        $email_data['otp']=$otp;
        $email_data['name']=$user_data->first_name;
        
        $val = Mail::send('mailformat/forgotpassword', $email_data, function($message) use($email,$subject) {
			$message->to($email);
			$message->subject($subject);
			$message->from(config('constants.EMAIL_FROM'));
        });
        return $val;
        
    }
    
    
    public function cronFireReminder(){
        $sql = \App\Models\Reminder::select('reminders.id','reminders.reminder_name','reminders.parent_id','users.first_name','reminders.child_id','child.first_name as child_first_name','child.device_id','exercises.exercise_name','durations.time_unit_id','durations.duration','reminders.minutes','reminders.next_reminder_datetime')
                ->leftjoin('users','users.id','=','reminders.parent_id')
                ->leftjoin('users as child','child.id','=','reminders.child_id')
                ->leftjoin('exercises','exercises.id','=','reminders.exercise_id')
                ->leftjoin('durations','durations.id','=','reminders.duration_id')
                ->where('reminders.reminder_status','0')
                ->where('reminders.minutes','>','0')
                ->whereNotNull('child.device_id')
                ->whereNull('child.deleted_at')
                ->whereNull('exercises.deleted_at')
                ->get();
        
        $reminder_arr = array();
        $current_date = date('Y-m-d H:i:s');
        if($sql->count()){
            foreach($sql as $value){
                log::info('Current Time: '. $current_date);
                $registrationIds = array();
                if ($current_date >= $value->next_reminder_datetime && !empty($value->device_id)) {

                    $reminder_arr[] = $value->id;
                    $push_notification = array();
                    $registrationIds = array($value->device_id);
                    $push_notification['not_title'] = trans('common.exercises_notification');
                    $push_notification['not_msg'] = $value->first_name.' '.trans('common.exercises_notification_cron_note').' '.$value->exercise_name;
                    $push_notification['registrationIds'] = $registrationIds;
                    $push_notification['reminder_id'] = $value->id;
                    ApiCommonController::pushNotification($push_notification);

                    $next_reminder_datetime = date('Y-m-d H:i', strtotime('+' . $value->minutes . ' minutes', strtotime($current_date)));
                    $reminder = \App\Models\Reminder::find($value->id);
                    $reminder->next_reminder_datetime = $next_reminder_datetime;
                    $reminder->save();
                }
            }
        }
        return 'success';
//        echo '<pre>'; print_r($reminder_arr); die;
    }
}
