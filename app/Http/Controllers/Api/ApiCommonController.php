<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
//use App\Apiuser;
//use App\Http\Controllers\Api\MailController;
use App;
use Illuminate\Support\Facades\Log;

class ApiCommonController extends Controller {

    protected $data = array();


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->data['file_url'] = URL::to('/public/') . '/';
        $this->data['upload_url'] = URL::to('/upload/') . '/';
    }

    public static function dateDMYconvertYMD($date) {
        $return_date = $date;
        if (!empty($date)) {
            $explode = explode('-', $date);
            if (isset($explode[0]) && isset($explode[1]) && isset($explode[2])) {
                $return_date = $explode[2] . '-' . $explode[1] . '-' . $explode[0];
            }
        }
        return $return_date;
    }

     public static function sendForgotPasswordEmail($email_data,$lang) {
        if($lang != ''){
            App::setLocale($lang);
        } else {
            App::setLocale('en');
        }
        $user_rp_token_created_date = date('Y-m-d H:i:s');
        $user_rp_token = md5($email_data->email) . '-' . md5(uniqid());
        $username = $email_data->name;

        $data = array();
        $data['TO'] = $email_data->email;
        $data['FROM'] = trans('api.SMTP_FROM');
        $data['SITE_NAME'] = trans('api.SITE_NAME');
        $data['SUBJECT'] = trans('api.label_post_forget_email_subject');
        $data['VIEW'] = 'mails.forgetpassword';
        $user_rp_token_url = URL::to('/api/resetpassword/' . $user_rp_token);
        $data['PARAM'] = array('name' => $username, 'user_rp_token_url' => $user_rp_token_url);
        $data['name'] = $username;
        $data['user_rp_token_url'] = $user_rp_token_url;

        $send_mail = MailController::send($data);

        $update_data = Apiuser::find($email_data->id);
        $update_data->forget_pass_token = $user_rp_token;
        $update_data->forget_pass_token_created_at = $user_rp_token_created_date;
        $update_data->save();
        return $send_mail;
    }

    public static function removeNullValue($data) {
        $return_array = array();
        if (!empty($data)) {
            foreach ($data as $key => $value) {

                if(strcmp($key,'created_at')==0 || strcmp($key,'updated_at')==0 || strcmp($key,'deleted_at')==0)
                {
                    $return_array[$key]=(!empty($value) || $value == '0')?date('d M, Y h:i A',strtotime($value)):'';
                }else{
                    $return_array[$key] = (!empty($value) || $value == '0') ? $value : '';
                }
            }
        }
        return (object) $return_array;
    }

    public static function uploadImage($field,$request,$folder){
        $file = $request->file($field);
        $name = $file->getClientOriginalName();
        $newFileName = time().'_'. $name;
        $destinationPath = $folder;
        $file->move($destinationPath,$newFileName);
        return $newFileName;
    } // end of function

     public static function uploadVideo($field,$request,$folder){
        $file = $request->file($field);
        $name = $file->getClientOriginalName();
        $newFileName = time().'_'. $name;
        $destinationPath = $folder;
        $file->move($destinationPath,$newFileName);
        return $newFileName;
    }

    public function generateOTP()
    {
        $count = 6;
        $temp_newKey = substr(number_format(time() * mt_rand(), 0, '', ''), 0, $count);
        if (strlen($temp_newKey) == 5) {
            $newKey = '0' . $temp_newKey;
        } else {
            $newKey = $temp_newKey;
        }
        return $newKey;

    } // end of function

    public function pushNotification($push_notification){
        $url = "https://fcm.googleapis.com/fcm/send";
        $serverKey = config('constants.FCM_SERVER_KEY');

        $notification = array('title' =>$push_notification['not_title'] , 'body' => $push_notification['not_msg'], 'sound' => 'default', 'badge' =>'1','reminder_id' => $push_notification['reminder_id'],'click_action'=>''); // com.hed.delivery.activity.HomeActivity

        $arrayToSend = array('registration_ids' => $push_notification['registrationIds'], 'notification'=>$notification,'priority'=>'high');
        log::info(print_r($arrayToSend,true));
        $json = json_encode($arrayToSend);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: key='. $serverKey;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close( $ch );
        return '1';
    }
    
     public static function getReminderData($page_no,$user,$reminder_id)
    {
        
        $res = \App\Models\Reminder::select('reminders.*', 'exercises.exercise_name', 'durations.duration', 'time_units.title', 'users.first_name as child_first_name', 'users.last_name as child_last_name', 'users1.first_name as parent_first_name', 'users1.last_name as parent_last_name', 'reminder_has_uploads.video', 'reminder_has_uploads.id as upload_id')
            ->leftjoin('exercises', 'reminders.exercise_id', '=', 'exercises.id')
            ->leftjoin('durations', 'reminders.duration_id', '=', 'durations.id')
            ->leftjoin('time_units', 'time_units.id', '=', 'durations.time_unit_id')
            ->leftjoin('reminder_has_uploads', 'reminders.id', '=', 'reminder_has_uploads.reminder_id')
            ->leftjoin('users', 'users.id', '=', 'reminders.child_id')
            ->leftjoin('users as users1', 'users1.id', '=', 'reminders.parent_id');

        if ($user->usertype == config('constants.USER_TYPE_CHILD')) {
            $res->where('reminders.child_id', $user->id)->where('reminders.parent_id', $user->parent_id);
        } else {
            $res->where('reminders.parent_id', $user->id);
        }
        if(!empty($reminder_id)){
            $res->where('reminders.id', $reminder_id);
        }
        $res->whereNull('users.deleted_at');
        $sql = $res->orderBy('reminders.id', 'desc');

        $count_sql_data = clone $sql;
        $total_records  = $count_sql_data->count();

        $last_page_number = 0;
        $limit = config('constants.COMMON_PAGINATION'); // limit
        $offset = 0; // offset start record for query

        if ($total_records) {
            $division = $total_records / $limit;
            $last_page_number = ceil($division); // nearest integer for last page number
        }

        if (!empty($limit) && !empty($page_no) && $page_no > 0) {
            $offset = (int) ($limit * ($page_no - 1)); // Counting for offset. Query start record number.
        }

        if ($last_page_number == $page_no || $last_page_number == 1) {
            $page_no = 0; // If last page number then we will give them this flag as 0. So they can not call again for more records.
        } else {
            $page_no++;
        }

        $sql_data = $sql->offset($offset)->limit($limit)->get();

        if ($sql_data->count()) {
            $folder = 'uploads/video/';
            foreach ($sql_data as $key => $t_data) {
                if (!empty($t_data->video)) {
                    $t_data->video = URL::to("/") . '/' . $folder . $t_data->video;
                }
                $temp_data = $t_data->toArray();
                $return_data[$key] = ApiCommonController::removeNullValue($temp_data);
            }
        } else {
             $page_no = 0;
            $return_data = array();
        }
        $temp_arr = array();
        $temp_arr['return_data'] = $return_data;
        $temp_arr['page_no'] = $page_no;
        $temp_arr['total_records'] = $total_records;
        return $temp_arr;
        
    }
}
