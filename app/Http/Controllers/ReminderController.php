<?php

namespace App\Http\Controllers;

use App\Models\Reminder;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ReminderController extends Controller
{
    public function index(Request $request){
//        $data=Reminder::with('exercise')
//            ->with('duration')
//            ->with('parent')
//            ->with('child')
//            ->with('video')
//            ->orderBy('created_at', 'desc')
//            ->get();

        $data = \App\Models\Reminder::select('reminders.id','reminders.reminder_name','reminders.parent_id','reminders.child_id','exercises.exercise_name','durations.time_unit_id','durations.duration','reminders.minutes','reminders.next_reminder_datetime','reminder_has_uploads.video','reminders.reminder_status')
                ->leftjoin('exercises','exercises.id','=','reminders.exercise_id')
                ->leftjoin('durations','durations.id','=','reminders.duration_id')
                ->leftjoin('reminder_has_uploads','reminder_has_uploads.reminder_id','=','reminders.id')              
                ->whereNull('exercises.deleted_at')
                ->orderBy('reminders.created_at', 'desc')
                ->get();
//        print'<pre>';
//        print_r($data);
//        die;
        if($request->ajax()){
            return DataTables::of($data)
                ->setRowId(function ($data) {
                    return $data->id;
                })
                ->addIndexColumn()
                ->EditColumn('reminder_status', function ($data) {
                    if ($data->reminder_status == 0) {
                        //return $data->status_id;
                        return "<span class='bg-danger  color-palette'> Pending </span>";
                    } else {
                        return "<span class='bg-success color-palette'> Completed </span>";
                    }
                })
                ->AddColumn('video', function ($data) {
                    if ($data->reminder_status == 1) {
                        //return $data->status_id;
                        return "<a target='_blank' href='". asset('/uploads/video/'.$data->video) ."' class='btn bg-gradient-primary'><i class='fa fa-video'></i></a>";
                    }
                })
                ->EditColumn('duration_id', function ($data) {
                    if ($data->time_unit_id == 1 ) {
                        //return $data->status_id;
                        return $data->duration." Minutes";
                    } else {
                        return $data->duration." Hour";
                    }
                })
                ->rawColumns(['reminder_status','video'])
                ->make(true);
        }

        return view('reminder.index');
    }
}
