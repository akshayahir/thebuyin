<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    //
    public function index(Request $request){
        return view('profile.index');
    }

    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            'first_name'=>'required | max:255',
            'last_name'=>'required | max:255',
            'email'=>'required | email | max:255 | unique:users,email,'.$request->user()->id.',id',
            'mobile'=>'required|regex:/^[0-9]{10}$/',
        ]);

        if ($validator->fails()) {
            $message = "<ul id='add-alert' class='alert alert-danger' style='padding-left:30px;'>";

            foreach ($validator->errors()->all() as $error) {
                $message .= "<li>$error</li>";
            }

            $message .= "</ul>";

            return response()->json([
                'message' => $message,
                'success' => 0
            ]);
        }

        if($request->password!=""){
            $validator=Validator::make($request->all(),[
                'password'=>'required|min:8|string|confirmed',
            ]);

            if ($validator->fails()) {
                $message = "<ul id='add-alert' class='alert alert-danger' style='padding-left:30px;'>";

                foreach ($validator->errors()->all() as $error) {
                    $message .= "<li>$error</li>";
                }

                $message .= "</ul>";

                return response()->json([
                    'message' => $message,
                    'success' => 0
                ]);
            }

        }

        $profile=Auth::user();

        $profile->first_name=$request->first_name;
        $profile->last_name=$request->last_name;
        $profile->mobile_number=$request->mobile;
        $profile->email=$request->email;

        if($request->password!=""){
            $profile->password=Hash::make($request->password);
        }

        $profile->save();


        return response()->json([
            'success' => 1
        ]);
    }
}
