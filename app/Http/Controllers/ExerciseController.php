<?php

namespace App\Http\Controllers;

use App\Models\Exercise;
use Illuminate\Http\Request;
use PHPUnit\Framework\Error\Notice;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class ExerciseController extends Controller
{
    //
    public function index(Request $request){
        $data=Exercise::orderBy('exercise_name', 'asc')->withTrashed()
            ->get();

        if($request->ajax()){
            return DataTables::of($data)
                ->setRowId(function ($data) {
                    return $data->id;
                })
                ->addIndexColumn()
                ->addColumn('action', function ($data) {
                    $output = '';
                    $output='
                        <div>
                            <button id="" class="btn bg-gradient-primary edit_model"
                            data-id="'. $data->id .'" data-name="'.$data->exercise_name.'">

                            <i class="fa fa-edit"></i>&nbsp; Edit </button>

                        ';

                    if ($data->deleted_at != NULL) {
                        $output.='

                            <button id="" class="btn  bg-gradient-danger restore_model"
                            data-id="'. $data->id .'">

                            <i class="fa fa-undo"></i>&nbsp; Restore </button>

                       ';
                    }else{
                        $output.='
                            <button id="" class="btn bg-gradient-danger delete_model"
                            data-id="'. $data->id .'">

                            <i class="fa fa-trash"></i>&nbsp; Delete </button>
                       ';
                    }

                    $output.='</div>';

                    return $output;
                })
                ->addColumn('status', function ($data) {
                    if ($data->deleted_at == null) {
                        //return $data->status_id;
                        return "<span class='bg-success  color-palette'> Active </span>";
                    } else {
                        return "<span class='bg-danger color-palette'> Deleted </span>";
                    }
                })
                ->rawColumns(['action','status'])
                ->blacklist(['action','status'])
                ->make(true);
        }

        return view('exercise.index');
    }

    public function add(Request $request){
        $validator=Validator::make($request->all(),[
            'name'=>'required'
        ]);

        if ($validator->fails()) {
            $message = "<ul id='add-alert' class='alert alert-danger' style='padding-left:30px;'>";

            foreach ($validator->errors()->all() as $error) {
                $message .= "<li>$error</li>";
            }

            $message .= "</ul>";

            return response()->json([
                'message' => $message,
                'success' => 0
            ]);
        }

        $exercise=new Exercise();

        $exercise->exercise_name=$request->name;

        $exercise->save();

        return response()->json([
            'success' => 1
        ]);
    }
    public function update(Request $request){
        $validator=Validator::make($request->all(),[
            'name'=>'required',
            'id'=>'required'
        ]);

        if ($validator->fails()) {
            
            $message = "<ul id='add-alert' class='alert alert-danger' style='padding-left:30px;'>";

            foreach ($validator->errors()->all() as $error) {
                $message .= "<li>$error</li>";
            }

            $message .= "</ul>";

            return response()->json([
                'message' => $message,
                'success' => 0
            ]);
        }
       
        $exercise=Exercise::where('id',$request->id)->withTrashed()->first();                
        $exercise->exercise_name=$request->name;
//echo '<pre>'; print_r($exercise); die;
        $exercise->save();

        return response()->json([
            'success' => 1
        ]);

    }

    public function delete(Request $request){
        Exercise::withTrashed()->where('id',$request->id)->delete();

        return response()->json([
            'success' => 1
        ]);
    }

    public function restore(Request $request){
        Exercise::withTrashed()->where('id',$request->id)->restore();

        return response()->json([
            'success' => 1
        ]);
    }
}
