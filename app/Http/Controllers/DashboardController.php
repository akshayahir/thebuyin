<?php

namespace App\Http\Controllers;

use App\Models\Exercise;
use App\User;
use Carbon\Carbon;
use Auth,DB,Session;
use App\Models\Reminder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class DashboardController extends CommonController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() {

        $auth = Auth::user();
        $return_data['users'] = $auth;
        $this->data['meta_title'] = "Dashboard";

//        $data=Reminder::with('exercise')
//            ->with('duration')
//            ->with('parent')
//            ->with('child')
//            ->where('reminder_status','0')
//            ->orderBy('created_at', 'desc')
//            ->take(5)
//            ->get();
        
        $data = \App\Models\Reminder::select('reminders.id','reminders.reminder_name','reminders.parent_id','reminders.child_id','exercises.exercise_name','durations.time_unit_id','durations.duration','reminders.minutes','reminders.next_reminder_datetime')                
                ->leftjoin('exercises','exercises.id','=','reminders.exercise_id')
                ->leftjoin('durations','durations.id','=','reminders.duration_id')
                ->where('reminders.reminder_status','0')               
                ->whereNull('exercises.deleted_at')
                ->orderBy('reminders.created_at', 'desc')
                ->get();

        $r_count=Reminder::get()->count();
        $p_count=Reminder::where('reminder_status','0')
            ->get()
            ->count();
        $e_count=Exercise::get()->count();
        $u_count=User::where('usertype','!=','A')
            ->get()->count();

        // dd($data);
//echo '<pre>'; print_r($data); die;
        return View('admin.dashboard.index', array_merge($this->data, $return_data),
        [
            'reminders'=>$data,
            'r_count'=>$r_count,
            'p_count'=>$p_count,
            'e_count'=>$e_count,
            'u_count'=>$u_count
        ])->render();
    }

}
