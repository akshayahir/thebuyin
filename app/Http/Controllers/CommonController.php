<?php

namespace App\Http\Controllers;

use Auth;
use Route;
use Session;
use DateTime;
use Hash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\URL;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\User;

class CommonController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $route = Route::current()->action;
        $route_group = '';
        $alias = '';
        if (isset($route['routegroup'])) {
            $route_group = $route['routegroup'];
        }
        
        if (isset($route['as'])) {
            $alias = $route['as'];
        }
        

        $this->data['route_group'] = $route_group;
        $this->data['alias'] = $alias;

        $this->data['uploadpath'] = URL::to('/upload/') . '/';
    }

    public function dateDMYconvertYMD($date) {
        $return_date = $date;
        if (!empty($date)) {
            $explode = explode('-', $date);
            if (isset($explode[0]) && isset($explode[1]) && isset($explode[2])) {
                $return_date = $explode[2] . '-' . $explode[1] . '-' . $explode[0];
            }
        }
        return $return_date;
    }
    
    public static function commonDateDMYconvertYMD($date) {
        $return_date = $date;
        if (!empty($date)) {
            $explode = explode('-', $date);
            if (isset($explode[0]) && isset($explode[1]) && isset($explode[2])) {
                $return_date = $explode[2] . '-' . $explode[1] . '-' . $explode[0];
            }
        }
        return $return_date;
    }
}
