<?php

//file : app/config/constants.php

$constants = [
    'SITE_NAME' => 'The Buy In',
    'AppName' => 'thebuyin',
    'NO_IMG' => 'no-image.jpg',
    'USER_TYPE_PARENT' => 'P',
    'USER_TYPE_CHILD' => 'C',
    'COMMON_PAGINATION' => '5',
    'COMMON_MINUTES' => '1',
    'COMMON_HOURS' => '2',
    'FCM_SERVER_KEY' => 'AAAAtUhC6KM:APA91bG--5KUBCa7IX4xgzbkrkfF4RsN6zuHMPbXHZA5BP-FyRFfBKgavWApmIEftUPYdPYwEkDpdcBDxXWUTpZ1NEU1a0nknfyBxAbHmgsXKVWFufdKxBrka_xfQq40wt-GPXcxiDek',
    'EMAIL_FROM' => env('MAIL_USERNAME'), 
];
return $constants;
