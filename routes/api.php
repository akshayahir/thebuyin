<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
    Route::get('user/get/profile', 'Api\UserController@profile');
    Route::post('user/profile/setup', 'Api\UserController@profileSetup');
    Route::post('user/change/password', 'Api\UserController@change_password');
    Route::post('user/logout', 'Api\UserController@logout');

    Route::post('addchild', 'Api\UserController@addChild');
    Route::post('deletechild', 'Api\UserController@deleteChild');

    Route::get('getexercises', 'Api\UserController@getExercises');
    Route::get('getdurationtime', 'Api\UserController@getDurationTime');
    Route::get('getchildren', 'Api\UserController@getChildren');
    Route::post('addreminder', 'Api\UserController@addReminder');
    Route::post('updatereminder', 'Api\UserController@updateReminder');
    Route::post('deletereminder', 'Api\UserController@deleteReminder');
    Route::get('getchildpendingreminder', 'Api\UserController@getChildPendingReminder');
    Route::post('uploadvideoreminder', 'Api\UserController@uploadVideoReminder');
    Route::post('getallreminderlist', 'Api\UserController@getAllReminderList');
});

Route::post('user/login', 'Api\UserController@login');
Route::post('user/register', 'Api\UserController@register');
Route::post('user/verifyotp', 'Api\UserController@verify_otp');
Route::post('user/resendotp', 'Api\UserController@resend_otp');
Route::post('user/forgotpassword', 'Api\UserController@forgotpassword');
Route::post('user/resetpassword', 'Api\UserController@resetpassword');
Route::get('cron-reminder', array('as' => 'cron.reminder', 'routegroup' => 'cron.reminder', 'uses' => 'Api\UserController@cronFireReminder'));
