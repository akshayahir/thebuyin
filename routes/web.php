<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();
Route::post('/postlogin', array('as' => 'postlogin', 'routegroup' => 'login', 'uses' => 'Auth\LoginController@postLogin'));
Route::get('/', array('as' => 'loginpage', 'routegroup' => 'login', 'uses' => 'Auth\LoginController@index'));


Route::prefix('admin')->group(function () {
    Route::group(array('middleware' => 'auth'), function() {
        Route::get('/log-out', array('as' => 'log-out', 'routegroup' => 'login', 'uses' => 'Auth\LoginController@postLogout'));
        Route::get('/home', array('as' => 'home', 'routegroup' => 'grp_dashboard', 'uses' => 'DashboardController@index'));
        Route::get('/dashboard', array('as' => 'dashboard', 'routegroup' => 'grp_dashboard', 'uses' => 'DashboardController@index'));

        Route::get('/profile', array('as' => 'profile.index', 'routegroup' => 'grp_dashboard', 'uses' => 'ProfileController@index'));

        Route::post('/profile/update', array('as' => 'profile.update', 'routegroup' => 'grp_dashboard', 'uses' => 'ProfileController@update'));

        Route::get('/exercise', array('as' => 'exercise.index', 'routegroup' => 'grp_dashboard', 'uses' => 'ExerciseController@index'));
        Route::post('/exercise/add', array('as' => 'exercise.add', 'routegroup' => 'grp_dashboard', 'uses' => 'ExerciseController@add'));
        Route::post('/exercise/update', array('as' => 'exercise.update', 'routegroup' => 'grp_dashboard', 'uses' => 'ExerciseController@update'));
        Route::post('/exercise/delete', array('as' => 'exercise.delete', 'routegroup' => 'grp_dashboard', 'uses' => 'ExerciseController@delete'));
        Route::post('/exercise/restore', array('as' => 'exercise.restore', 'routegroup' => 'grp_dashboard', 'uses' => 'ExerciseController@restore'));

//        Route::get('/duration', array('as' => 'duration.index', 'routegroup' => 'grp_dashboard', 'uses' => 'DurationController@index'));
//        Route::post('/duration/add', array('as' => 'duration.add', 'routegroup' => 'grp_dashboard', 'uses' => 'DurationController@add'));
//        Route::post('/duration/update', array('as' => 'duration.update', 'routegroup' => 'grp_dashboard', 'uses' => 'DurationController@update'));
//        Route::post('/duration/delete', array('as' => 'duration.delete', 'routegroup' => 'grp_dashboard', 'uses' => 'DurationController@delete'));
//        Route::post('/duration/restore', array('as' => 'duration.restore', 'routegroup' => 'grp_dashboard', 'uses' => 'DurationController@restore'));

        Route::get('/user', array('as' => 'user.index', 'routegroup' => 'grp_dashboard', 'uses' => 'UserController@index'));
        Route::get('/user/child/{id}', array('as' => 'user.child', 'routegroup' => 'grp_dashboard', 'uses' => 'UserController@view_child'));

        Route::get('/reminder', array('as' => 'reminder.index', 'routegroup' => 'grp_dashboard', 'uses' => 'ReminderController@index'));

    });
});
