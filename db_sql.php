<?php

CREATE TABLE `durations` (
  `id` int(11) UNSIGNED NOT NULL,
  `time_in` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '''1'' = Minutes, ''2'' = Hours',
  `duration` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `durations` (`id`, `time_in`, `duration`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 15, '2020-04-26 00:00:00', '2020-04-26 00:00:00', NULL),
(2, '2', 3, '2020-04-26 00:00:00', '2020-04-26 00:00:00', NULL),
(3, '1', 45, '2020-04-26 00:00:00', '2020-04-26 00:00:00', NULL),
(4, '2', 1, '2020-04-26 00:00:00', '2020-04-26 00:00:00', NULL),
(5, '1', 30, '2020-04-26 00:00:00', '2020-04-26 00:00:00', NULL);

ALTER TABLE `durations`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `durations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;



CREATE TABLE `time_units` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `time_units` (`id`, `title`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Minutes', '2020-04-28 23:14:17', '2020-04-28 23:14:17', NULL),
(2, 'Hours', '2020-04-28 23:14:17', '2020-04-28 23:14:17', NULL);

 
ALTER TABLE `time_units`
  ADD PRIMARY KEY (`id`);
 
ALTER TABLE `time_units`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `durations` CHANGE `time_in` `time_unit_id` ENUM('1','2') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '\'1\' = Minutes, \'2\' = Hours';

    
ALTER TABLE `reminders` CHANGE `reminder_name` `child_id` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `reminders` ADD `parent_id` INT(11) NOT NULL DEFAULT '0' AFTER `id`;
ALTER TABLE `reminders` CHANGE `duration` `duration_id` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `reminders` CHANGE `time_in` `exercise_id` INT(11) NOT NULL DEFAULT '0';
ALTER TABLE `reminders` ADD `reminder_name` VARCHAR(255) NULL AFTER `id`;
ALTER TABLE `reminders` ADD `respond_status` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT '0 = Pending, 1 = Completed' AFTER `duration_id`;
ALTER TABLE `reminders` CHANGE `respond_status` `reminder_status` ENUM('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0 = Pending, 1 = Completed';
    
CREATE TABLE `reminder_has_uploads` (
  `id` int(11) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `child_id` int(11) NOT NULL DEFAULT '0',
  `reminder_id` int(11) NOT NULL DEFAULT '0',
  `video` varchar(255)  DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
  
ALTER TABLE `reminder_has_uploads`
  ADD PRIMARY KEY (`id`);
 
ALTER TABLE `reminder_has_uploads`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `reminders` ADD `next_reminder_datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `reminder_status`;
ALTER TABLE `reminders` ADD `minutes` INT(11) NOT NULL DEFAULT '0' AFTER `reminder_status`;
?>
