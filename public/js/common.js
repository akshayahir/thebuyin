$(document).ready(function () {
    $('form').find('input[type=text],textarea,select').filter(':input:visible:first').focus();

    // Alpha Numeric
    $(document).on('keypress', '.number_only', function (e) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    });

    // Integer
    $(document).on('keypress', '.integer_only', function (e) {
        var regex = new RegExp("^[0-9]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }
        e.preventDefault();
        return false;
    });

    $(document).on('keypress', '.price-validation', function (event) {
        element = $(this);
        element.data("oldValue", '').bind("paste", function (e) {
            // var validNumber = /^[-]?\d+(\.\d{1,2})?$/;
            element.data('oldValue', element.val())
            setTimeout(function () {
                if (element.val()<1)
                    element.val("");
            }, 0);
        });

        element.data("oldValue", '').bind("paste", function (e) {
            var validNumber = /^[-]?\d+(\.\d{1,2})?$/;
            element.data('oldValue', element.val())
            setTimeout(function () {
                if (!validNumber.test(element.val()))
                    element.val(element.data('oldValue'));
            }, 0);
        });


        var text = $(this).val();

        if (event.which == 45) {
            event.preventDefault(); //cancel the keypress
        }
        if ((event.which != 46 || text.indexOf('.') != -1) && //if the keypress is not a . or there is already a decimal point
                ((event.which < 48 || event.which > 57) && //and you try to enter something that isn't a number
                        (event.which != 45 || (element[0].selectionStart != 0 || text.indexOf('-') != -1)) && //and the keypress is not a -, or the cursor is not at the beginning, or there is already a -
                        (event.which != 0 && event.which != 8))) { //and the keypress is not a backspace or arrow key (in FF)
            event.preventDefault(); //cancel the keypress
        }

        if ((text.indexOf('.') != -1) && (text.substring(text.indexOf('.')).length > 2) && //if there is a decimal point, and there are more than two digits after the decimal point
                ((element[0].selectionStart - element[0].selectionEnd) == 0) && //and no part of the input is selected
                (element[0].selectionStart >= element.val().length - 2) && //and the cursor is to the right of the decimal point
                (event.which != 45 || (element[0].selectionStart != 0 || text.indexOf('-') != -1)) && //and the keypress is not a -, or the cursor is not at the beginning, or there is already a -
                (event.which != 0 && event.which != 8)) { //and the keypress is not a backspace or arrow key (in FF)
            event.preventDefault(); //cancel the keypress
        }
    });

    $('#login_form').validate({
        rules: {
            email: {required: true, email: true},
            password: {required: true}
        },
        messages: {
            email: {required: langauge_var.login_email, email: langauge_var.login_valid_email},
            password: {required: langauge_var.common_password},
        },
        showErrors: function (errorMap, errorList) {
            var error = [];
            $.each(errorMap, function (key, value) {
                error.push(value);
                if (error.length == 1) {
                    $('#' + key).focus();
                }
            });
            if (error.length != 0) {
                $.toast({
                    heading: langauge_var.common_error_header,
                    text: error,
                    icon: 'error',
                    hideAfter: 5000,
                    position: 'toast-bottom-right'
                });
            }
        },
        submitHandler: function (form) {
            $('#LoadingImage').show();
            form.submit();
        },
        onkeyup: false,
        focusInvalid: false,
        onfocusout: false, //Disables onblur validation.
        onclick: false //Disables onclick validation of checkboxes and radio buttons.
    });

    $(document).on('change', '.image-upload-5mb', function (e) {
        var val = $(this).val();
        var file_size = this.files[0].size;
        var default_upload_size = 5 * 1024 * 1024;//5MB size
        var ext = val.substring(val.lastIndexOf('.') + 1).toLowerCase();
        switch (ext) {
            case 'gif':
            case 'jpg':
            case 'png':
            case 'jpeg' :

                $(this).parent().removeClass('has-error');
                break;
            default:
                $.toast({
                    heading: langauge_var.common_error_header,
                    text: langauge_var.common_invalid_file_format,
                    icon: 'error',
                    hideAfter: 5000,
                    position: 'toast-bottom-right'
                });                
                $(this).val('');
                $(this).filestyle('clear');
                break;
        }
        //to validate the image size
        if (file_size > default_upload_size) {
            $.toast({
                    heading: langauge_var.common_error_header,
                    text: langauge_var.common_file_size_5mb,
                    icon: 'error',
                    hideAfter: 5000,
                    position: 'toast-bottom-right'
                });            
            $(this).val('');
            $(this).filestyle('clear');
            return false;
        }
    });
    $(document).on('keypress', '.price_validate', function (e) {
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        if (($(this).val().indexOf('.') != -1) && ($(this).val().substring($(this).val().indexOf('.'), $(this).val().indexOf('.').length).length > 2)) {
            event.preventDefault();
        }
    });
    $(".numbers_value").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $(".numbers_value").keyup(function (e) {
        var str = $(this).val();
        if (str == '0') {
            $(this).val('');
            return false;
        }
    });
    
    $(".allownumericwithoutdecimal").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $(".allownumericwithdecimal").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^0-9\.]/g, ''));
        if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $('.input_not_allow').keydown(function () {
        return false;
    });
});

function deleteconfirm(str) {
    if (confirm(str)) {
        return true;
    }
    return false;
}

function AvoidSpace(event) {
    var k = event ? event.which : window.event.keyCode;
    if (k == 32)
        return false;
}