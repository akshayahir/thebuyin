<footer class="main-footer">
    <strong>Copyright ©{{ date('Y') }} {{ Config::get('constants.SITE_NAME') }}. All rights reserved.
  </footer>
