@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/public/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{ asset('/public/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/css/responsive.bootstrap4.min.css') }}">

    <style>
        .error{
            border-color: red;
        }
        label.error{
            color: red;
            margin-top: 8px;
            margin-left: 8px;
        }
        span.error{
            color: red;
        }
    </style>
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Edit Profile</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">Edit Profile</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                    <div class="alert alert-success" id="flash_msg" style="display:none;">
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">

                    <div id="edit_errors">

                    </div>
                    <form id="edit-profile">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">First Name<span class="error">*</span></label>
                                    <input type="text" value="{{ Auth::user()->first_name }}" name="first_name" id="first_name" class="form-control" placeholder="Enter first name">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Last Name<span class="error">*</span></label>
                                    <input type="text" value="{{ Auth::user()->last_name }}" name="last_name" id="last_name" class="form-control" placeholder="Enter last name">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email<span class="error">*</span></label>
                                    <input type="email" value="{{ Auth::user()->email }}" name="email" id="email" class="form-control" placeholder="Enter email">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mobile Number<span class="error">*</span></label>
                                    <input type="number" value="{{ Auth::user()->mobile_number }}" name="mobile" id="mobile" class="form-control" placeholder="Enter mobile number">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Password</label>
                                    <input type="password"  name="password" id="password" class="form-control" placeholder="Enter password">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Confirm Password</label>
                                    <input type="password" name="confirm" id="confirm" class="form-control" placeholder="Enter confirm password">
                                </div>
                            </div>
                        </div>
                        <button type="submit" id="edit_btn" data-id="" class="btn btn-primary">Update</button>
                    </form>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

    </section>
</div>
@endsection


@section('scripts')
    <script src="{{ asset('/public/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/public/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/public/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/public/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/public/js/sweetalert.min.js')}}"></script>
    <script>
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $("#edit-profile").validate({
                rules:{
                    first_name:{
                        required:true,
                        maxlength:255
                    },
                    last_name:{
                        required:true,
                        maxlength:255
                    },
                    email:{
                        required:true,
                        email:true,
                        maxlength:255
                    },
                    mobile:{
                        required:true,
                        mobile_valid:$("#mobile").val(),
                        maxlength:10

                    },
                    password:{
                        required:function(){
                            return ($("#confirm").val()!="")
                        },
                        minlength:8,
                        maxlength:255
                    },
                    confirm:{
                        equalTo:'#password',
                        maxlength:255,
                        minlength:8,
                    },
                },
                messages:{
                        first_name:{
                            required:"Enter the First name",
                            maxlength:"Length should be less than 255"
                        },
                        last_name:{
                            required:"Enter the Last name",
                            maxlength:"Length should be less than 255"
                        },
                        email:{
                            required:"Enter the email",
                            email:"Enter valid email",
                            maxlength:"Length should be less than 255"
                        },
                        mobile:{
                            required:"Enter the mobile number",
                            mobile_valid:"Enter valid mobile number",
                            maxlength:"Length should be less than 255"
                        },
                        password:{
                            required:"Enter the password",
                            maxlength:"Length should be less than 255",
                            minlength:"Minimum 8 letters required"

                        },
                        confirm:{
                            equalTo:"Confirm password and password should match",
                            maxlength:"Length should be less than 255"
                        }
                },
                submitHandler:function(form){
                    var f_name=$("#first_name").val();
                    var l_name=$("#last_name").val();
                    var email=$("#email").val();
                    var mobile=$("#mobile").val();
                    var password=$("#password").val();
                    var confirm=$("#confirm").val();

                    $.ajax({
                        method:'POST',
                        url:"{{ route('profile.update') }}",
                        data:{
                            'first_name':f_name,
                            'last_name':l_name,
                            'email':email,
                            'mobile':mobile,
                            'password':password,
                            'password_confirmation':confirm
                        },
                        dataType: 'JSON',
                        success:function(response){
                            console.log(response);
                            if (response.success == 0) {
                                $("#edit_errors").html(response.message);
                            }else{
                                $("#flash_msg").html("Profile Updated");

                                $("#flash_msg").show();

                                setTimeout(function() {
                                    $("#flash_msg").hide();
                                    window.location.href="{{ route('profile.index') }}";

                                }, 1000);

                            }
                        }
                    });
                    return false;
                }
            });

            $.validator.addMethod('mobile_valid', function (value) {
                return /^\d{10}$/.test(value);
            }, 'Please enter a valid mobile number.');


        });

    </script>

@endsection
