@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <link rel="stylesheet" href="{{ asset('/public/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/css/responsive.bootstrap4.min.css') }}">

    <style>
        .error{
            border-color: red;
        }
        label.error{
            color: red;
            margin-top: 8px;
            margin-left: 8px;
        }
    </style>
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Duration</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">Duration</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                    <div class="alert alert-success" id="flash_msg" style="display:none;">
                    </div>

                    <button id="add-duration" style="float:right" class="btn bg-gradient-primary">

                    <i class="fa fa-plus"></i>&nbsp; ADD </button>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Time Unit</th>
                      <th>Duration</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

        <div id="add-modal" class="modal fade" data-backdrop="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="modal-title text-md">Add Duration</div>
                        <button class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div id="add_errors">
                        </div>
                        <form id="add-duration-form">

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Time Unit</label>

                                    <select id="time_unit" name="time_unit" class="form-control">
                                        <option value="">Select Time unit</option>
                                        <option value="1">Minutes</option>
                                        <option value="2">Hours</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Duration</label>
                                    <input type="number" maxlength="11" name="duration" id="duration" class="form-control" id="exampleInputEmail1" min="0" placeholder="Enter Duration">
                                </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                        <button type="submit" id="add_btn" class="btn btn-primary">Add</button>
                    </div>
                </form>
                </div>
            </div>
        </div>

        <div id="edit-modal-lg" class="modal fade" data-backdrop="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="modal-title text-md">Edit Duartion</div>
                        <button class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div id="edit_errors">
                        </div>
                        <form id="edit_form">

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Time Unit</label>

                                    <select id="edit_time_unit" name="edit_time_unit" class="form-control">
                                        <option value="">Select Time unit</option>
                                        <option value="1">Minutes</option>
                                        <option value="2">Hours</option>
                                    </select>
                                </div>
                                 <div class="form-group">
                                    <label for="exampleInputEmail1">Duration</label>
                                    <input type="number" maxlength="11" min="0" id="edit_duration" name="edit_duration" class="form-control" id="exampleInputEmail1" placeholder="Enter Duration">
                                </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                        <button type="submit" id="edit_btn" data-id="" class="btn btn-primary">Update</button>
                    </div>
                </form>
                </div>
            </div>
        </div>

    </section>
</div>
@endsection


@section('scripts')
    <script src="{{ asset('/public/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/public/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/public/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/public/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js"></script>

    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var table=$('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            "serverSide": true,
            "processing": true,
            "ajax":{
                url:"{{ route('duration.index') }}"
            },
            columnDefs: [{
                "Sortable": false,
                "orderable": false,
                "targets": 0
            }],
            columns: [
                { data: "DT_RowIndex" },
                { data:'time_unit',name:'time_unit' },
                { data:'duration',name:'duration' },
                { data: 'status', name: 'status', orderable: false },
                { data:'action',name:'action',orderable:false}
            ]
          });


        $("#add-duration").on("click",function(){
            $("#add-modal").modal('toggle');
            $("#add_errors").innerHTML="";

            $("label.error").html("");

            $("#time_unit").val('');
            $("#time_unit").removeClass('error');
            $("#duration").removeClass('error');

        });

        $("#add-duration-form").validate({
            rules:{
                time_unit:{
                    required:true,
                },
                duration:{
                    required:true,
                    digits:true,
                    maxlength:11
                }
            },
            messages:{
                time_unit:{
                    required:"Select Time Unit",
                },
                duration:{
                    required:'Enter the duration',
                    digits:'Enter digits only',
                    maxlength:"Length should be less then or equal to 11"
                }
            },
            submitHandler:function(form){
                var duration= $("#duration").val();
                var time= $("#time_unit").val();

                $.ajax({
                    method:'POST',
                    url:"{{ route('duration.add') }}",
                    data:{
                        'time_unit':time,
                        'duration':duration
                    },
                    dataType: 'JSON',
                    success:function(response){

                        if (response.success == 0) {
                            $("#add_errors").html(response.message);
                        }else{
                            $("#duration").val("");
                            $("#time_unit").val("");

                            $("#add-modal").modal('toggle');

                            window.scrollTo(0, 0);

                            $("#flash_msg").html("Duration Added");

                            $("#flash_msg").show();

                            setTimeout(function() {
                                $("#flash_msg").hide();
                            }, 1000);
                            table.ajax.reload(null,false);
                        }
                    }
                });
            }
        });

        $("#edit_form").validate({
            rules:{
                edit_time_unit:{
                    required:true,
                },
                edit_duration:{
                    required:true,
                    digits:true,
                    maxlength:11
                }
            },
            messages:{
                edit_time_unit:{
                    required:"Select Time Unit",
                },
                edit_duration:{
                    required:'Enter the duration',
                    digits:'Enter digits only',
                    maxlength:"Length should be less then or equal to 11"
                }
            },
            submitHandler:function(form){
                var id=$("#edit_btn").data('id');
                var duration= $("#edit_duration").val();
                var time= $("#edit_time_unit").val();

                $.ajax({
                    method:'POST',
                    url:"{{ route('duration.update') }}",
                    data:{
                        'id':id,
                        'duration':duration,
                        'time_unit':time
                    },
                    dataType: 'JSON',
                    success:function(response){
                        if (response.success == 0) {
                            $("#edit_errors").html(response.message);
                        }else{

                            $("#edit-modal-lg").modal('toggle');

                            window.scrollTo(0, 0);

                            $("#flash_msg").html("Duration Updated");

                            $("#flash_msg").show();

                            setTimeout(function() {
                                 $("#flash_msg").hide();
                            }, 1000);
                            table.ajax.reload(null,false);

                        }
                    }
                });

            }
        });

        $(document).on("click",".edit_model",function(){
            var id=$(this).data('id');
            var time=$(this).data('time');
            var duration=$(this).data('duration');

            $("#edit_time_unit").val(time);

            $("#edit_duration").val(duration);

            $("#edit_btn").attr('data-id',id);

            $("#edit-modal-lg").modal('toggle');

            $("#edit_errors").html("");

            $("label.error").html("");

            {{--  $("#edit_time_unit").val('');  --}}

            $("#edit_time_unit").removeClass('error');
            $("#edit_duration").removeClass('error');

        });


        $(document).on("click",".restore_model",function(){
            swal({
                title:"Restore duration",
                text:"Are you sure you want to restore ?",
                type:'warning',
                icon:'warning',
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if(!willDelete){
                    return;
                }
                var id=$(this).data('id');

                $.ajax({
                    'type':'POST',
                    'url':"{{ route('duration.restore') }}",
                    'data':{
                        'id':id
                    },
                    'dataType':'JSON',
                    success:function(response){
                        if(response.success==1){
                            window.scrollTo(0, 0);

                            $("#flash_msg").html("Duration Restored");

                            $("#flash_msg").show();

                            setTimeout(function() {
                                $("#flash_msg").hide();
                            }, 1000);
                            table.ajax.reload(null,false);

                        }
                    },
                    error:function (xhr, ajaxOptions, thrownError) {
                        swal("Error!", "Please try again", "error");
                    }
                });
            });
        });


        $(document).on("click",".delete_model",function(){
            swal({
                title:"Delete duration",
                text:"Are you sure you want to delete ?",
                type:'warning',
                icon:'warning',
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if(!willDelete){
                    return;
                }
                var id=$(this).data('id');

                $.ajax({
                    'type':'POST',
                    'url':"{{ route('duration.delete') }}",
                    'data':{
                        'id':id
                    },
                    'dataType':'JSON',
                    success:function(response){
                        if(response.success==1){
                            window.scrollTo(0, 0);

                            $("#flash_msg").html("Duration Deleted");

                            $("#flash_msg").show();

                            setTimeout(function() {
                                $("#flash_msg").hide();
                            }, 1000);
                            table.ajax.reload(null,false);

                        }
                    },
                    error:function (xhr, ajaxOptions, thrownError) {
                        swal("Error!", "Please try again", "error");
                    }
                });
            });
        });
    </script>

@endsection


        {{--  $("#add_btn").on("click",function(){
            var duration= $("#duration").val();
            var time= $("#time_unit").val();

            $.ajax({
                method:'POST',
                url:"{{ route('duration.add') }}",
                data:{
                    'time_unit':time,
                    'duration':duration
                },
                dataType: 'JSON',
                success:function(response){
                    if (response.success == 0) {
                        $("#add_errors").html(response.message);
                    }else{
                        $("#add-modal").modal('toggle');

                        window.scrollTo(0, 0);

                        $("#flash_msg").html("Duration Added");

                        $("#flash_msg").show();

                        setTimeout(function() {
                            $("#flash_msg").hide();
                            location.reload();
                        }, 1000);
                    }
                }
            });


        });  --}}

        {{--  $("#edit_btn").on("click",function(){
            var id=$(this).data('id');
            var duration= $("#edit_duration").val();
            var time= $("#edit_time_unit").val();

            $.ajax({
                method:'POST',
                url:"{{ route('duration.update') }}",
                data:{
                    'id':id,
                    'duration':duration,
                    'time_unit':time
                },
                dataType: 'JSON',
                success:function(response){
                    if (response.success == 0) {
                        $("#edit_errors").html(response.message);
                    }else{
                        $("#edit-modal-lg").modal('toggle');

                        window.scrollTo(0, 0);

                        $("#flash_msg").html("Duration Updated");

                        $("#flash_msg").show();

                        setTimeout(function() {
                             $("#flash_msg").hide();
                             location.reload();
                        }, 1000);

                    }
                }
            });

        });  --}}
