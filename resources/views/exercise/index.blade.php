@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/public/css/ionicons.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/public/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/css/responsive.bootstrap4.min.css') }}">

    <style>
        .error{
            border-color: red;
        }
        label.error{
            color: red;
            margin-top: 8px;
            margin-left: 8px;
        }
    </style>

@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Exercises</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">Exercises</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                    <div class="alert alert-success" id="flash_msg" style="display:none;">
                    </div>

                    <button id="add-exercise" style="float:right" class="btn bg-gradient-primary">

                    <i class="fa fa-plus"></i>&nbsp; ADD </button>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Exccerise Name</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

        <div id="add-modal" class="modal fade" data-backdrop="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="modal-title text-md">Add Exercise</div>
                        <button class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div id="add_errors">
                        </div>
                        <form id="excercise-add-form">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Exercise Name</label>
                                    <input type="text" name="name" maxlength="255" id="name" class="form-control" placeholder="Enter exercise name">
                                </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                        <button type="submit" id="add_btn" class="btn btn-primary">Add</button>
                    </div>
                </form>
                </div>
            </div>
        </div>

        <div id="edit-modal-lg" class="modal fade" data-backdrop="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="modal-title text-md">Edit Exercise</div>
                        <button class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div id="edit_errors">
                        </div>
                        <form id="edit-exercise">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Exercise Name</label>
                                    <input type="text" maxlength="255" name="edit_name" id="edit_name" class="form-control" id="exampleInputEmail1" placeholder="Enter exercise name">
                                </div>

                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="id_edit" name="id_edit" value="" />
                        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                        <button type="submit" id="edit_btn" data-id="" class="btn btn-primary">Update</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
@endsection


@section('scripts')
    <script src="{{ asset('/public/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/public/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/public/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/public/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/public/js/sweetalert.min.js')}}"></script>

    <script>

        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });




            var table=$('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                 "serverSide": true,
                 "processing": true,
                "ajax":{
                    url:"{{ route('exercise.index') }}"
                },
                columnDefs: [{
                    "Sortable": true,
                    "orderable": false,
                    "targets": 0
                }],
                columns: [
                    { data: "DT_RowIndex" },
                    { data:'exercise_name',name:'exercise_name' },
                    { data: 'status', name: 'status', orderable: false },
                    { data:'action',name:'action',orderable:false}
                ]
            });

            $("#add-exercise").on("click",function(){
                $("#add-modal").modal('toggle');
                $("#add_errors").innerHTML="";

                $(".error").html("");
                $("#name").removeClass('error');
            });

            $("#excercise-add-form").validate({
                rules:{
                    name:{
                        required:true,
                        maxlength:255
                    }
                },
                messages:{
                    name:{
                        required:"Enter the Exercise name",
                        maxlength:"Length should be less than 255"
                    }
                },
                submitHandler:function(form){
                    var name= $("#name").val();
                    $.ajax({
                        method:'POST',
                        url:"{{ route('exercise.add') }}",
                        data:{
                            'name':name
                        },
                        dataType: 'JSON',
                        success:function(response){
                            if (response.success == 0) {
                                $("#add_errors").html(response.message);
                            }else{
                                $("#add-modal").modal('toggle');

                                window.scrollTo(0, 0);

                                $("#name").val("");

                                $("#flash_msg").html("Exercise Added");

                                $("#flash_msg").show();

                                setTimeout(function() {
                                    $("#flash_msg").hide();
                                }, 1000);

                                table.ajax.reload(null,false);
                            }
                        }
                    });
                }
            });

            $("#edit-exercise").validate({
                rules:{
                    edit_name:{
                        required:true,
                        maxlength:255,
                    }
                },
                messages:{
                    edit_name:{
                        required:"Enter the Exercise name",
                        maxlength:"Length should be less than 255",
                    }
                },
                submitHandler:function(form){
                    var id=$("#id_edit").val();
//                    alert(id);
                    var name= $("#edit_name").val();
//                    alert(name);
                    $.ajax({
                        method:'POST',
                        url:"{{ route('exercise.update') }}",
                        data:{
                            'id':id,
                            'name':name
                        },
                        dataType: 'JSON',
                        success:function(response){
                            if (response.success == 0) {
                                $("#edit_errors").html(response.message);
                            }else{
                                $("#edit-modal-lg").modal('toggle');

                                window.scrollTo(0, 0);

                                $("#flash_msg").html("Exercise Updated");

                                $("#flash_msg").show();
                                $("#edit_name").val("");

                                setTimeout(function() {
                                    $("#flash_msg").hide();
                                }, 1000);

                                table.ajax.reload(null,false);
                            }
                        }
                    });

                }
            });

            $(document).on("click",".edit_model",function(){
                var id=$(this).data('id');
                var name=$(this).data('name');
//alert(id);
                $("#edit_name").val(name);

                $("#id_edit").val(id);

                $("#edit-modal-lg").modal('toggle');

                $("#edit_errors").html("");

                $(".error").html("");
                $("#edit_name").removeClass('error');


            });


            $(document).on("click",".restore_model",function(){
                swal({
                    title:"Restore exercise",
                    text:"Are you sure you want to restore ?",
                    type:'warning',
                    icon:'warning',
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if(!willDelete){
                        return;
                    }
                    var id=$(this).data('id');

                    $.ajax({
                        'type':'POST',
                        'url':"{{ route('exercise.restore') }}",
                        'data':{
                            'id':id
                        },
                        'dataType':'JSON',
                        success:function(response){
                            if(response.success==1){
                                window.scrollTo(0, 0);

                                $("#flash_msg").html("Exercise Restored");

                                $("#flash_msg").show();

                                setTimeout(function() {
                                    $("#flash_msg").hide();
                                }, 1000);
                                table.ajax.reload(null,false);

                            }
                        },
                        error:function (xhr, ajaxOptions, thrownError) {
                            swal("Error!", "Please try again", "error");
                        }
                    });
                });
            });


            $(document).on("click",".delete_model",function(){
                swal({
                    title:"Delete exercise",
                    text:"Are you sure you want to delete ?",
                    type:'warning',
                    icon:'warning',
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if(!willDelete){
                        return;
                    }
                    var id=$(this).data('id');

                    $.ajax({
                        'type':'POST',
                        'url':"{{ route('exercise.delete') }}",
                        'data':{
                            'id':id
                        },
                        'dataType':'JSON',
                        success:function(response){
                            if(response.success==1){
                                window.scrollTo(0, 0);

                                $("#flash_msg").html("Exercise Deleted");

                                $("#flash_msg").show();

                                setTimeout(function() {
                                    $("#flash_msg").hide();
                                }, 1000);
                                table.ajax.reload(null,false);

                            }
                        },
                        error:function (xhr, ajaxOptions, thrownError) {
                            swal("Error!", "Please try again", "error");
                        }
                    });
                });
            });

        });
    </script>

@endsection
