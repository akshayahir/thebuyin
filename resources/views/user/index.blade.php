@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/public/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{ asset('/public/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/css/responsive.bootstrap4.min.css') }}">

@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Users</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">Users</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">

                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>First Name</th>
                      <th>Last name</th>
                      <th>Mobile</th>
                      <th>Email</th>
                      <th>View child</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

    </section>
</div>
@endsection


@section('scripts')
    <script src="{{ asset('/public/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/public/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/public/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/public/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/public/js/sweetalert.min.js')}}"></script>


    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            "serverSide": true,
            "processing": true,
            "ajax":{
                url:"{{ route('user.index') }}"
            },
            columnDefs: [{
                "Sortable": false,
                "orderable": false,
                "targets": 0
            }],
            columns: [
                { data: "DT_RowIndex" },
                { data:'first_name',name:'first_name' },
                { data:'last_name',name:'last_name' },
                { data:'mobile_number',name:'mobile_number' },
                { data:'email',name:'email' },
                { data:'action',name:'action' },
             ]
          });

    </script>

@endsection
