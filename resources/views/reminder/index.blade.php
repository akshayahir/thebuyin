@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/public/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{ asset('/public/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/public/css/responsive.bootstrap4.min.css') }}">

@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Reminders</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li class="breadcrumb-item active">Reminders</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">

                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>ID</th>
                      <th>Reminder Name</th>
                      <th>Exercise</th>
                      <th>Duration</th>
                      <th>Status</th>
                      <th>Video</th>

                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

    </section>
</div>
@endsection


@section('scripts')
    <script src="{{ asset('/public/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/public/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/public/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/public/js/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/public/js/sweetalert.min.js')}}"></script>


    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            "responsive": true,
            "serverSide": true,
            "processing": true,
            "ajax":{
                url:"{{ route('reminder.index') }}"
            },
            columnDefs: [{
                "Sortable": false,
                "orderable": false,
                "targets": 0
            }],
            columns:[
                { data: "DT_RowIndex" },
                { data:'reminder_name',name:'reminder_name' },
                { data:'exercise_name',name:'exercise_name' },
                { data:'duration_id',name:'duration_id' },
                { data:'reminder_status',name:'reminder_status' },
                { data:'video',name:'video'},

             ]
          });

    </script>

@endsection
