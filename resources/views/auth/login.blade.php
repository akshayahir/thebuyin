@extends('layouts.without_login_app')

@section('content')

<div class="login-box">
  <div class="login-logo">
    <img src="{{ asset('/public/img/logo.png') }}" alt="{{ trans('common.SITE_NAME') }}" class="brand-image">
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form id="login_form" method="POST" action="{{ route('postlogin') }}">
          @csrf
        <div class="input-group mb-3">
           <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
         <input id="password" type="password" class="form-control" name="password" required autocomplete="current-password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">

          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <p class="mb-1">
        {{-- <a href="forgot-password.html">I forgot my password</a> --}}
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>


@endsection
