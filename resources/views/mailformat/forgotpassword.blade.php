<p style="font-size: 15px;">Hello {{$name}},<br><br>

<p style="font-size: 15px;"> There has recently been a change request your account password.</p>
<p style="font-size: 15px;"> If you requested this change of password, please reset your password using OTP: <strong>{{$otp}}</strong><br> 
<p style="font-size: 15px;"> If you have not made this request, you can ignore this message and your password will remain the same.</p><br>
<p style="font-size: 15px;">Thank You</p>