<p style="font-size: 15px;">Hello {{$name}},<br><br>

<p style="font-size: 15px;"> Thanks for getting started with {{config('constants.Appinfo.AppName')}}.</p>
<p style="font-size: 15px;"> Please verify your account using OTP: <strong>{{$otp}}</strong><br>
<p style="font-size: 15px;">Thank You</p>