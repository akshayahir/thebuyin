<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ trans('common.SITE_NAME') }}</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('/public/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/public/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/public/css/toaster.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
    @yield('content')
    <script type="text/javascript">
        var base_url = "{{URL::to('/').'/'}}";
        var langauge_var = {!! json_encode(trans('javascript')); !!};
    </script>    
    
    <script type="text/javascript">
        var stack_bar_bottom = {"dir1": "up", "dir2": "right", "spacing1": 0, "spacing2": 0};
    </script>
    <!-- Jquery JS-->
    <script src="{{ asset('/public/js/jquery.min.js') }}"></script> 
    <script src="{{ asset('/public/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/public/js/adminlte.js') }}"></script>
    <script src="{{ asset('/public/js/jquery-ui.min.js') }}"></script> 
    <script src="{{ asset('/public/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('/public/js/additional-methods.js') }}"></script>

    <script src="{{ asset('/public/js/toaster.js') }}"></script>
    <script src="{{ asset('/public/js/common.js') }}"></script>

    <?php
    $success_load_msg = session('success');
    $error_load_msg = session('error');
    ?>
<script type="text/javascript">
            var success_load_msg = "{{$success_load_msg}}";
            var error_load_msg = "{{$error_load_msg}}";
            if(success_load_msg){
                $.toast({
                    heading: langauge_var.common_success_header,
                    text: success_load_msg,
                    icon: 'success',
                    hideAfter: 5000,
                    position: 'toast-bottom-right'
                });                
            }
            if(error_load_msg){
                $.toast({
                    heading: langauge_var.common_error_header,
                    text: error_load_msg,
                    icon: 'error',
                    hideAfter: 5000,
                    position: 'toast-bottom-right'
                });
            }
        </script>
</body>
</html>