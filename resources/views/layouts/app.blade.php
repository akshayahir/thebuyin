<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="csrf-token" id="csrf-token" content="{{ csrf_token() }}">

        <title>{{ trans('common.SITE_NAME') }}</title>

        <!-- Font Awesome Icons -->
        <link rel="stylesheet" href="{{ asset('/public/css/all.min.css') }}">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="{{ asset('/public/css/OverlayScrollbars.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('/public/css/adminlte.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/public/css/toaster.css') }}">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

        @yield('styles')

    </head>
    <body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
        <div class="wrapper">
            <script type="text/javascript">
                var base_url = "{{URL::to('/').'/'}}";
                var langauge_var = {!! json_encode(trans('javascript')); !!};
            </script>
            <div id="LoadingImage" style="display: none;">
                <img src="{{ asset('/public/img/message-loader.gif') }}">
            </div>
            @include('admin/includes/header')

            @include('admin/includes/sidebar')

            @yield('content')

            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>

            @include('admin/includes/footer')
        </div>
        <!-- ./wrapper -->

        <!-- REQUIRED SCRIPTS -->
        <!-- jQuery -->
        <script src="{{ asset('/public/js/jquery.min.js') }}"></script>
        <!-- Bootstrap -->
        <script src="{{ asset('/public/js/bootstrap.bundle.min.js') }}"></script>
        <!-- overlayScrollbars -->
        <script src="{{ asset('/public/js/jquery.overlayScrollbars.min.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ asset('/public/js/adminlte.js') }}"></script>

        <!-- OPTIONAL SCRIPTS -->
        <script src="{{ asset('/public/js/jquery.validate.min.js') }}"></script>
        <script src="{{ asset('/public/js/additional-methods.js') }}"></script>

        <script src="{{ asset('/public/js/toaster.js') }}"></script>
        <script src="{{ asset('/public/js/common.js') }}"></script>

        @yield('scripts')

        <!-- PAGE SCRIPTS -->
    </body>
</html>
