<?php

return [
    'SITE_NAME' => 'The Buy In',
    'SITE_NAME_HEADER' => 'The Buy In',
    'SITE_FIRST' => 'The Buy In',
    'SITE_LAST' => 'TBI',
    'REMINDER_STATUS_PENDING' => '0',
    'REMINDER_STATUS_COMPLETED' => '1',
    'exercises_notification' => 'Exercise reminder',
    'exercises_notification_note' => 'has sent you exercise reminder',
    'exercises_notification_cron_note' => 'has sent you reminder for exercise',
    'exercises_notification_for_delete' => 'has deleted your exercise reminder',
    'exercises_notification_for_update' => 'has updated your exercise reminder',
    'FORGOTPASSWORD_SUBJECT' => 'The Buy In: Forgot Password',
    'REGISTRATION_SUBJECT' => 'The Buy In: User Verification',
    'exercises_created' => 'Exercise created',
    'exercises_updated' => 'Exercise updated',
    'exercises_deleted' => 'Exercise deleted',
    'exercises_created_not' => 'has created reminder for exercise',
    'exercises_updated_not' => 'has updated reminder for exercise',
    'exercises_deleted_not' => 'has deleted reminder for exercise',
    
];
